package com.uom.msc.ai.crpto.security;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.SignatureException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/** Class to handle user authorization */
public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private static final Logger log = LoggerFactory.getLogger(JwtAuthorizationFilter.class);

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws IOException, ServletException {
        UsernamePasswordAuthenticationToken authentication = getAuthentication(request);
        if (authentication == null) {
            filterChain.doFilter(request, response);
            return;
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {

        //retrieve the token from request header
        String token = request.getHeader(SecurityConstants.TOKEN_HEADER);
        if (StringUtils.isNotEmpty(token) && token.startsWith(SecurityConstants.TOKEN_PREFIX)) {
            try {
                byte[] signingKey = SecurityConstants.JWT_SECRET.getBytes();

                Jws<Claims> parsedToken = Jwts.parser()
                    .setSigningKey(signingKey)
                    .parseClaimsJws(token.replace("Bearer ", ""));

                String username = parsedToken
                    .getBody()
                    .getSubject();

                List<SimpleGrantedAuthority> authorities = ((List<?>) parsedToken.getBody()
                    .get("rol")).stream()
                    .map(authority -> new SimpleGrantedAuthority((String) authority))
                    .collect(Collectors.toList());

                Object userId = (String)parsedToken.getBody()
                        .get("userId");

                if (StringUtils.isNotEmpty(username)) {

                    //create the user based on token and add to spring security context
                    return new UsernamePasswordAuthenticationToken(userId, null, authorities);
                }
            } catch (ExpiredJwtException exception) {

                log.warn("Request to parse expired JWT : {} failed : {}", token, exception.getMessage());
                throw exception;
            } catch (UnsupportedJwtException exception) {

                log.warn("Request to parse unsupported JWT : {} failed : {}", token, exception.getMessage());
                throw exception;
            } catch (MalformedJwtException exception) {

                log.warn("Request to parse invalid JWT : {} failed : {}", token, exception.getMessage());
                throw exception;
            } catch (SignatureException exception) {

                log.warn("Request to parse JWT with invalid signature : {} failed : {}", token, exception.getMessage());
                throw exception;
            } catch (IllegalArgumentException exception) {

                log.warn("Request to parse empty or null JWT : {} failed : {}", token, exception.getMessage());
                throw exception;

            }
        }

        return null;
    }
}
