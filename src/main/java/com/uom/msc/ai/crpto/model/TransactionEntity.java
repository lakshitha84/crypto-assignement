package com.uom.msc.ai.crpto.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "user_transaction")
@Getter
@Setter
public class TransactionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "user_id")
    private Integer userId;

    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    private TranType tranType;

    private String account;
}
