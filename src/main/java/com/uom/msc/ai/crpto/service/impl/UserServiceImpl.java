package com.uom.msc.ai.crpto.service.impl;

import com.uom.msc.ai.crpto.model.TransactionEntity;
import com.uom.msc.ai.crpto.model.UserEntity;
import com.uom.msc.ai.crpto.repository.TransactionRepository;
import com.uom.msc.ai.crpto.repository.UserRepository;
import com.uom.msc.ai.crpto.security.SecurityUser;
import com.uom.msc.ai.crpto.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.transaction.UserTransaction;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<UserEntity> optionalUserEntity = userRepository.findByUsername(username);
        if (optionalUserEntity.isPresent()) {

            UserEntity entity = optionalUserEntity.get();
            System.out.println(entity.getRoles().size());
            return new SecurityUser(entity);
        } else {

            throw new UsernameNotFoundException("" + username);
        }
    }

    @Override
    public UserEntity createUser(UserEntity userEntity) {

        userEntity.setPassword(passwordEncoder.encode(userEntity.getPassword()));
        UserEntity saved = userRepository.save(userEntity);
        return saved;
    }

    @Override
    public List<TransactionEntity> getTrans(Integer userId) {

        return transactionRepository.findAllByUserId(userId);
    }
}
