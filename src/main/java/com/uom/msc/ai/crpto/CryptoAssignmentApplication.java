package com.uom.msc.ai.crpto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CryptoAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(CryptoAssignmentApplication.class, args);
	}

}
