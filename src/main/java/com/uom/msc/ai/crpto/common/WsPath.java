package com.uom.msc.ai.crpto.common;

/** The class contains the api urls*/
public interface WsPath {

    /** API version 1 */
    static final String API_V_1 = "/api/1";

    static final String USERS = API_V_1 + "/users";

    static final String AUTH = API_V_1 + "/auth";
}
