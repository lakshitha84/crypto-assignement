package com.uom.msc.ai.crpto.enums;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
