package com.uom.msc.ai.crpto.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uom.msc.ai.crpto.payload.JwtRequest;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
/** The filter calls for authenticating username and password and returning the JWT toeken to access the APIs */
public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    /** The static final logger for logging */
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthenticationFilter.class);

    /** Spring authentication manager */
    private final AuthenticationManager authenticationManager;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
        //configure the authentication url
        setFilterProcessesUrl(SecurityConstants.AUTH_LOGIN_URL);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {

        try {

            ObjectMapper mapper = new ObjectMapper();
            //Retrieve the user credentials from request
            JwtRequest jwtRequest =  mapper.readValue(request.getReader(), JwtRequest.class);
            Authentication authenticationToken = new UsernamePasswordAuthenticationToken(jwtRequest.getUsername(), jwtRequest.getPassword());
            //Pass the authentication token to spring security context
            return authenticationManager.authenticate(authenticationToken);
        } catch (RuntimeException ex) {

            throw ex;
        } catch (Exception ex) {

            LOGGER.info("Exception", ex);
            throw new SecurityException();
        }

    }

    //The method is to handle login success scionario
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain filterChain, Authentication authentication) {


        SecurityUser user = ((SecurityUser) authentication.getPrincipal());
        List<String> roles = user.getAuthorities()
            .stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.toList());

        byte[] signingKey = SecurityConstants.JWT_SECRET.getBytes();

        //Build the jwt token
        String token = Jwts.builder()
            .signWith(Keys.hmacShaKeyFor(signingKey), SignatureAlgorithm.HS512)
            .setHeaderParam("typ", SecurityConstants.TOKEN_TYPE)
            .setIssuer(SecurityConstants.TOKEN_ISSUER)
            .setAudience(SecurityConstants.TOKEN_AUDIENCE)
            .setSubject(user.getUsername())
            .setExpiration(new Date(System.currentTimeMillis() + 864000000))
            .claim("rol", roles)
                .claim("userId", user.getUserId())
            .compact();
        //Add the generated token to response header
        response.addHeader(SecurityConstants.TOKEN_HEADER, SecurityConstants.TOKEN_PREFIX + token);
    }
}
