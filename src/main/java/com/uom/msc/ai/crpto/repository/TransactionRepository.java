package com.uom.msc.ai.crpto.repository;

import com.uom.msc.ai.crpto.model.TransactionEntity;
import com.uom.msc.ai.crpto.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionEntity, Integer> {

   List<TransactionEntity> findAllByUserId(Integer userId);
}
