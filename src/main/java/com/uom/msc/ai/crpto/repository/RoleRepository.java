package com.uom.msc.ai.crpto.repository;

import com.uom.msc.ai.crpto.enums.RoleName;
import com.uom.msc.ai.crpto.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByName(RoleName roleName);
}
