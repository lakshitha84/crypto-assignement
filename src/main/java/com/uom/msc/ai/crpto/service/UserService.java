package com.uom.msc.ai.crpto.service;

import com.uom.msc.ai.crpto.model.TransactionEntity;
import com.uom.msc.ai.crpto.model.UserEntity;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.transaction.UserTransaction;
import java.util.List;

public interface UserService extends UserDetailsService {

    /**
     * the service to create user
     * @param user
     * @return
     */
    UserEntity createUser(UserEntity user);

    /**
     * The service to get transactions
     * @param userId
     * @return
     */
    List<TransactionEntity> getTrans(Integer userId);
}
