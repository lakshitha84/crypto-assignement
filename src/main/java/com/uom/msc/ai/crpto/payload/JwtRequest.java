package com.uom.msc.ai.crpto.payload;

import lombok.Getter;
import lombok.Setter;

public class JwtRequest {

    @Getter @Setter
    private String username;

    @Getter @Setter
    private String password;
}
