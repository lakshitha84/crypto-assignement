package com.uom.msc.ai.crpto.security;

import com.uom.msc.ai.crpto.model.UserEntity;
import lombok.Data;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class SecurityUser extends User {

    private String userId;

    private Integer id;

    public SecurityUser(String username, String password, String userId, Integer id) {

        super(username, password, Collections.emptyList());
        this.userId = userId;
        this.id = id;
    }

    public SecurityUser(UserEntity entity) {

        super(entity.getUsername(), entity.getPassword(), entity.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName().name())).collect(Collectors.toList()));
        this.userId = userId;
        this.id = id;
    }
}
