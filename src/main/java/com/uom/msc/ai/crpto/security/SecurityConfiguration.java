package com.uom.msc.ai.crpto.security;


import com.uom.msc.ai.crpto.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@EnableWebSecurity //enable spring security
@EnableGlobalMethodSecurity(
        securedEnabled = true, //enable role bases method access
        jsr250Enabled = true,
        prePostEnabled = true
)
@Configuration
/** The class to configure spring security */
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    /*@Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;*/

    @Autowired
    private UserService userService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and()
            .csrf().disable()
            .authorizeRequests()
            .antMatchers( "/users/test", "/api/1/auth").permitAll()
            .anyRequest().authenticated()
            .and()
            .addFilter(new JwtAuthenticationFilter(authenticationManager())) //configure authentication filter
            .addFilter(new JwtAuthorizationFilter(authenticationManager()))//configure authorization filter
              //  .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
               // .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {

       auth.userDetailsService(userService);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {

        //configure password encoder
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());

        return source;
    }
}
