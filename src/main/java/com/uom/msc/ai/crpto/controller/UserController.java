package com.uom.msc.ai.crpto.controller;

import com.uom.msc.ai.crpto.model.TransactionEntity;
import com.uom.msc.ai.crpto.model.UserEntity;
import com.uom.msc.ai.crpto.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
/** The class t interface rest APIs*/
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/test")
    public String test() {

        return "tsting";
    }

    @PostMapping("/create") //Request mapping
    @Secured("ROLE_ADMIN") //Configure the role of the user to access the end point. Required the ROLE_ADMIN
    public UserEntity createUser(@RequestBody UserEntity userEntity) {

        UserEntity created = userService.createUser(userEntity);
        return created;
    }

    @GetMapping("/{userId}/transactions")  //Request mapping
    @Secured("ROLE_USER") //Configure the role of the user to access the end point. Required the ROLE_USER
    public List<TransactionEntity> getTransactions(@PathVariable(name = "userId") Integer userId) {

        return userService.getTrans(userId);
    }
}
